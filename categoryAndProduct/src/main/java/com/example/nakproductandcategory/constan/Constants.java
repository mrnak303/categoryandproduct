package com.example.nakproductandcategory.constan;

import com.example.nakproductandcategory.models.request.ItemKeyValue;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static List<ItemKeyValue> getAllStatus(){
        List<ItemKeyValue> itemKeyValues = new ArrayList<>();
        itemKeyValues.add(new ItemKeyValue(1,"ACT","Active"));
        itemKeyValues.add(new ItemKeyValue(2,"DEL","Delete"));
        itemKeyValues.add(new ItemKeyValue(3,"DSL","Disable"));
        return itemKeyValues;
    }
    public static List<String> getAllStatusString(){
        List<String> statustList = new ArrayList<>();
        statustList.add("ACT");
        statustList.add("DEL");
        statustList.add("DSL");
        return statustList;
    }

}
