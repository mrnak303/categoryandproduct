package com.example.nakproductandcategory.models;

import com.example.nakproductandcategory.models.request.ItemKeyValue;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity(name="products")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int code;
    private String barcode;
    private double cost;
    private double price;
    private double qty;
    private String description;
    private String stockType;
    private String status;
    @ManyToOne
    private  Category category;
    @Transient
    private List<Category> categories;
    @Transient
    private List<ItemKeyValue> statusList;

}
