package com.example.nakproductandcategory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NakproductandcategoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(NakproductandcategoryApplication.class, args);
    }

}
